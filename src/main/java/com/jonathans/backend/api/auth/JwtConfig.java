package com.jonathans.backend.api.auth;

public class JwtConfig{
        public static final String RSA_PUBLIC = "-----BEGIN PUBLIC KEY-----\n" +
                "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDk0hojsKuI8+iAiGnbVIahSdl\n" +
                "K3zVTwZS2hfgugpL7w03uY5U8auInqfDYOdwXPgr/RmpJ4xvKxCdibO4luJ/62UD\n" +
                "kwV592Ta8bZ1p5TACbWm5qBZKpTK16E0CY+2qFkNqO4tEq5HRnbDh7T1LRTUEhx/\n" +
                "D/atlA5QcK2JhScdLwIDAQAB\n" +
                "-----END PUBLIC KEY-----";

        public static final String RSA_PRIVATE = "-----BEGIN RSA PRIVATE KEY-----\n" +
                "MIICXAIBAAKBgQDDk0hojsKuI8+iAiGnbVIahSdlK3zVTwZS2hfgugpL7w03uY5U\n" +
                "8auInqfDYOdwXPgr/RmpJ4xvKxCdibO4luJ/62UDkwV592Ta8bZ1p5TACbWm5qBZ\n" +
                "KpTK16E0CY+2qFkNqO4tEq5HRnbDh7T1LRTUEhx/D/atlA5QcK2JhScdLwIDAQAB\n" +
                "AoGBALMQUqeIri6Kkzyd/vowWY/2gg9bNbyH0h5hmKMlMvA7OzreJbcYXhmESc3V\n" +
                "F5IFTSPZZHk/wJC4OUpEHczQOSeULj3olEF0wp0JBjsXEtOfMbSC9zB67XnRwkKn\n" +
                "/d5tYp3F2W4KusC3PvOfNazeNP2nZ5DjRZT9oRREIMcDzArBAkEA45wgiD1mjvSp\n" +
                "tn5EMbgGZpdw2QOIVCiKNWTv0PfHNtGU2wGabG3kmSDJ8i86zjbEpzZ9UKHPrZy8\n" +
                "1hO58t1CyQJBANv4QHsdXFMbJamV3YsrO23RE4J6hDRPHlF2w+4MjQM/Tiw27YXO\n" +
                "ebT/6xYSS+OLQkVq1/nWs0G8Q6nwMz+tpDcCQBCSubux3Nnj7sefAhAsuiqWNF1Y\n" +
                "6IBYQ1gH9X5dzF+tew1HXJswlUihHpNH5RxG2KLkUNOiNKELnKRp40xP2CECQD8z\n" +
                "juNQxkwiiqm1I5hu7X5aCi79IKse6GkjpJvrA0EhmUZe6dP+RT3aIiOPUjqosnvb\n" +
                "fP6Nz6ddflt/Zlove/UCQEe1/1a5FH2+fwE20oj2XcEIwu4A34+DdKwSSiZzpu60\n" +
                "kM8x2u+/8OE9QXy3V2I0gwq+q4LhU051mcVGmEOa1eE=\n" +
                "-----END RSA PRIVATE KEY-----";
}
