package com.jonathans.backend.api.models.entity;



import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "facturas")
public class Factura implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String description;

  private String observation;

  @Column(name = "create_at")
  @Temporal(TemporalType.DATE)
  private Date createAt;

  /** Muchas facturas están asociadas a 1 solo cliente */
  @JsonIgnoreProperties({"facturas", "hibernateLazyInitializer", "handler"})
  @ManyToOne(fetch = FetchType.LAZY)
  private Cliente cliente;

  @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "factura_id")
  private List<ItemFactura> items;


  /** Se le asigna la fecha de hoy */
  @PrePersist
  public void prePersist(){
    this.createAt = new Date();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getObservation() {
    return observation;
  }

  public void setObservation(String observation) {
    this.observation = observation;
  }

  public Date getCreateAt() {
    return createAt;
  }

  public void setCreateAt(Date createAt) {
    this.createAt = createAt;
  }

  public Cliente getCliente() {
    return cliente;
  }

  public void setCliente(Cliente cliente) {
    this.cliente = cliente;
  }

  public List<ItemFactura> getItems() { return items; }

  public void setItems(List<ItemFactura> items) { this.items = items; }

  public Double getTotal(){
    Double total;
    total = 0.00;
    for (ItemFactura item: items ) {
        total += item.getImporte();
    }
    return total;
  }

  public Factura() {
    items = new ArrayList<>();
  }

}
