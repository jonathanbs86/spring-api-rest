package com.jonathans.backend.api.models.entity;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "clientes")
public class Cliente implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(nullable = false)
  @NotBlank(message = " es requerido")
  private String nombre;

  @NotBlank(message = " es requerido")
  private String apellido;

  @Column(nullable = false, unique = true)
  @NotBlank(message = " es requerido")
  @Email
  private String email;

  @Column(name = "create_at")
  @Temporal(TemporalType.DATE)
  @NotNull(message = " no puede ser vacío")
  private Date createAt;

  private String photo;

  /** Un cliente tiene muchas facturas
   *  Si guardamos un cliente con facturas primero va a guardar e insertar el cliente y luego insertar las facturas
   * */
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente", cascade = CascadeType.ALL)
  private List<Factura> facturas;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getApellido() {
    return apellido;
  }

  public void setApellido(String apellido) {
    this.apellido = apellido;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Date getCreateAt() {
    return createAt;
  }

  public void setCreateAt(Date createAt) {
    this.createAt = createAt;
  }

  public String getPhoto() {
    return photo;
  }

  public void setPhoto(String photo) {
    this.photo = photo;
  }

  public List<Factura> getFacturas() { return facturas;  }

  public void setFacturas(List<Factura> facturas) { this.facturas = facturas; }

  public Cliente(){
    this.facturas = new ArrayList<>();
  }
}
