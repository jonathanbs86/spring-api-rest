package com.jonathans.backend.api.models.dao;

import com.jonathans.backend.api.models.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IClienteDao extends JpaRepository<Cliente, Long> {

}
