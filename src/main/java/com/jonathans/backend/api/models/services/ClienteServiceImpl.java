package com.jonathans.backend.api.models.services;

import com.jonathans.backend.api.models.dao.IClienteDao;
import com.jonathans.backend.api.models.dao.IFacturaDao;
import com.jonathans.backend.api.models.entity.Cliente;
import com.jonathans.backend.api.models.entity.Factura;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClienteServiceImpl implements IClienteService {

  @Autowired
  private IClienteDao clienteDao;

  @Autowired
  private IFacturaDao facturaDao;

  @Override
  @Transactional(readOnly = true)
  public List<Cliente> findAll() {
    List<Cliente> clienteList = (List<Cliente>) clienteDao.findAll();
    return clienteList;
  }

  @Override
  @Transactional(readOnly = true)
  public Page<Cliente> findAll(Pageable pageable) {
    return clienteDao.findAll(pageable);
  }

  @Override
  @Transactional(readOnly = true)
  public Cliente findById(Long id) {
    return clienteDao.findById(id).orElse(null);
  }

  @Override
  @Transactional
  public Cliente save(Cliente cliente) {
    return clienteDao.save(cliente);
  }

  @Override
  @Transactional
  public void delete(Long id) {
    clienteDao.deleteById(id);
  }

  @Override
  @Transactional(readOnly = true)
  public Factura findFacturaById(Long id) {
    return facturaDao.findById(id).orElse(null);
  }

  @Override
  @Transactional
  public Factura saveFactura(Factura factura) {
    return facturaDao.save(factura);
  }

  @Override
  @Transactional
  public void deleteFacturaById(Long id) {
    facturaDao.deleteById(id);
  }
}
