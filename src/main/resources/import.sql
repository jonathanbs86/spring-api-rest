INSERT INTO clientes (nombre, apellido, email, create_at) values ("Jonathan", "Brull Schroeder", "jonathans@avalith.net", NOW());
INSERT INTO clientes (nombre, apellido, email, create_at) values ("Ezequiel", "Morales", "ezequielm@avalith.net", NOW());
INSERT INTO clientes (nombre, apellido, email, create_at) values ("Damian", "Tacconi", "damiant@avalith.net", NOW());
INSERT INTO clientes (nombre, apellido, email, create_at) values ("Rodrigo", "Zamora", "rodrigoz@avalith.net", NOW());
INSERT INTO clientes (nombre, apellido, email, create_at) values ("Cristian", "Baldini", "cristianb@avalith.net", NOW());
INSERT INTO clientes (nombre, apellido, email, create_at) values ("Melina", "DiTomaso", "melinam@avalith.net", NOW());
INSERT INTO clientes (nombre, apellido, email, create_at) values ("Luis", "Hoyos", "luish@avalith.net", NOW());
INSERT INTO clientes (nombre, apellido, email, create_at) values ("Christian", "Ordosgoisti", "christiano@avalith.net", NOW());
INSERT INTO clientes (nombre, apellido, email, create_at) values ("Matias", "Gonzalez Trigo", "matiasgt@avalith.net", NOW());
INSERT INTO clientes (nombre, apellido, email, create_at) values ("Enzo", "Barucco", "enzob@avalith.net", NOW());
INSERT INTO clientes (nombre, apellido, email, create_at) values ("Francisco", "Barone", "franciscob@avalith.net", NOW());

INSERT INTO usuarios (username, password, enabled, nombre, apellido, email) values ("jonathans", "$2a$10$qzs1hadZi9xVJ8vdbPfefuxtnAKUMUfxLrnbDpow41RSGz4LI43KK", 1, "Jonathan", "Calleri", "jcalleri@avalith.net" );
INSERT INTO usuarios (username, password, enabled, nombre, apellido, email) values ("user2", "$2a$10$zxISIKEvERFWgGE9LK6xo.5JJBnoel0cR8i2OY41LkJFYqc/XdwtS", 1, "Juan Segundo", "Usuari" "jsusuari@avalith.net" );
INSERT INTO usuarios (username, password, enabled, nombre, apellido, email) values ("user3", "$2a$10$mj9bOlULHDiVRJOMJCT1oejpn7FaJCi3eEieBGc9X/EdxgncaLtQi", 1, "Yvael", "Tercero", "yvaeltercero@avalith.net");

INSERT INTO roles (nombre) values ("ROLE_ADMIN");
INSERT INTO roles (nombre) values ("ROLE_USER");

INSERT INTO usuarios_x_roles (usuario_id, role_id) values (1,1);
INSERT INTO usuarios_x_roles (usuario_id, role_id) values (1,2);
INSERT INTO usuarios_x_roles (usuario_id, role_id) values (2,2);
INSERT INTO usuarios_x_roles (usuario_id, role_id) values (3,2);

INSERT INTO productos (nombre, precio, create_at) VALUES ('Teclado hyperX', 14500, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES ('Mouse logitech G203', 2500 , NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES ("Monitor 14' FH600 " , 45000, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES ('Gabinete Sentey', 11500 ,NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES ('Auriculares Phillipos T405', 4500 ,NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES ('Placa de video RX5700XT', 50000 ,NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES ('Memoria RAM Corsair 16GB 2x8', 10000 ,NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES ('Motherboard Tuf Gaming X570 plus', 27500 ,NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES ('Procesador Ryzen 7 3800x', 32500 ,NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES ('RGB Fan coolers x5', 12000 ,NOW());

INSERT INTO facturas (description, observation, cliente_id, create_at) VALUES ('Factura equipos de oficina', null, 1, NOW());
INSERT INTO factura_items (cantidad, factura_id, producto_id) VALUES (5, 1, 1);
INSERT INTO factura_items (cantidad, factura_id, producto_id) VALUES (1, 1, 3);
INSERT INTO factura_items (cantidad, factura_id, producto_id) VALUES (2, 1, 2);
INSERT INTO factura_items (cantidad, factura_id, producto_id) VALUES (3, 1, 4);
INSERT INTO factura_items (cantidad, factura_id, producto_id) VALUES (2, 1, 6);


INSERT INTO facturas (description, observation, cliente_id, create_at) VALUES ('Factura RAM', "important note", 5, NOW());
INSERT INTO factura_items (cantidad, factura_id, producto_id) VALUES (2, 2, 7);