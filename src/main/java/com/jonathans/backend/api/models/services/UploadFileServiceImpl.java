package com.jonathans.backend.api.models.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class UploadFileServiceImpl implements IUploadFileService{
    private final Logger log = LoggerFactory.getLogger(UploadFileServiceImpl.class);
    private final static String UPLOAD_PATH = "uploads";

    @Override
    public Resource load(String namePhoto) throws MalformedURLException {
        Path rutaArchivo = getPath(namePhoto);
        log.info(rutaArchivo.toString());

        Resource recurso = null;

        recurso = (Resource) new UrlResource(rutaArchivo.toUri());

        if (!recurso.exists() && !recurso.isReadable()){
            rutaArchivo = Paths.get("src/main/resources/static/images").resolve("no-usuario.png").toAbsolutePath();

            recurso = (Resource) new UrlResource(rutaArchivo.toUri());

            log.error("No se pudo cargar la imagen: ");
        }
        return  recurso;
    }

    @Override
    public String copy(MultipartFile file) throws IOException {
        String nombreArchivo = UUID.randomUUID().toString()+"_"+file.getOriginalFilename().replace(" ","-");
        Path archivoRuta = getPath(nombreArchivo);
        log.info(archivoRuta.toString());

        Files.copy(file.getInputStream(),archivoRuta);

        return nombreArchivo;
    }

    @Override
    public Boolean delete(String namePhoto) {
        if (namePhoto !=null && namePhoto.length()>0){
            Path rutaFotoAnterior= Paths.get("uploads").resolve(namePhoto).toAbsolutePath();
            File archivoFotoAnterior = rutaFotoAnterior.toFile();
            if (archivoFotoAnterior.exists() && archivoFotoAnterior.canRead()){
                archivoFotoAnterior.delete();
                return true;
            }
        }
        return false;
    }

    @Override
    public Path getPath(String namePhoto) {
        return Paths.get(UPLOAD_PATH).resolve(namePhoto).toAbsolutePath();
    }
}
