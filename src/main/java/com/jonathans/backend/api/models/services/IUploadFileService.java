package com.jonathans.backend.api.models.services;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;

public interface IUploadFileService {

  public Resource load(String namePhoto) throws MalformedURLException;

  public String copy(MultipartFile file) throws IOException;

  public Boolean delete(String namePhoto);

  public Path getPath(String namePhoto);
}
