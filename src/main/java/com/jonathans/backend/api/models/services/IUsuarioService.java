package com.jonathans.backend.api.models.services;

import com.jonathans.backend.api.models.entity.Usuario;

public interface IUsuarioService {
    public Usuario findByUserName(String username);

}
