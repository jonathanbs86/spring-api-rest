package com.jonathans.backend.api.auth;

import com.jonathans.backend.api.models.entity.Usuario;
import com.jonathans.backend.api.models.services.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class TokenAditionalInfo implements TokenEnhancer {
  @Autowired private IUsuarioService usuarioService;

  @Override
  public OAuth2AccessToken enhance(
      OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
    Usuario usuario = usuarioService.findByUserName(oAuth2Authentication.getName());

    Map<String, Object> info = new HashMap<>();
    info.put(
        "info_aditional",
        "Saludando desde auth adicional a: ".concat(oAuth2Authentication.getName()));

    info.put(
            "nombre",
            usuario.getNombre());

    info.put(
            "apellido",
            usuario.getApellido());

    info.put(
            "email",
            usuario.getEmail());

    ((DefaultOAuth2AccessToken) oAuth2AccessToken).setAdditionalInformation(info);

    return oAuth2AccessToken;
  }
}
