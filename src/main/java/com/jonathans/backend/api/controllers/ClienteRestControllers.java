package com.jonathans.backend.api.controllers;

import com.jonathans.backend.api.models.entity.Cliente;
import com.jonathans.backend.api.models.services.IClienteService;


import com.jonathans.backend.api.models.services.IUploadFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.stream.Collectors;

/** @CrossOrigin(origins = {"http://localhost:4200"}) -> para conectar con Angular * */
@RestController
@RequestMapping("/api")
public class ClienteRestControllers {
  @Autowired IClienteService clienteService;

  @Autowired private IUploadFileService uploadFileService;

  private final Logger log = LoggerFactory.getLogger(ClienteRestControllers.class);

  @GetMapping("/clientes")
  public List<Cliente> getAllClientes() {
    return clienteService.findAll();
  }

  @GetMapping("/clientes/page/{page}")
  public Page<Cliente> getAllClientes(@PathVariable Integer page) {
    Pageable pageable = PageRequest.of(page, 4);
    return clienteService.findAll(pageable);
  }

  //@Secured({"ROLE_ADMIN","ROLE_USER"})
  @GetMapping("/clientes/{id}")
  public ResponseEntity<?> showCliente(@PathVariable Long id) {
    Cliente cliente = null;
    Map<String, Object> response = new HashMap<>();

    try {
      cliente = clienteService.findById(id);
    } catch (DataAccessException e) {
      response.put("mensaje", "Error al consultar en la base de datos!");
      response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
      return  new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    if (cliente == null){
      response.put("mensaje", "El cliente con ID ".concat(id.toString()).concat(" no existe en la base de datos"));
      return  new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
    }

    return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
  }

  @Secured("ROLE_ADMIN")
  @PostMapping("/clientes")
  public ResponseEntity<?> createCliente(@Valid @RequestBody Cliente cliente, BindingResult result) {
    Cliente clienteNew = null;
    Map<String, Object> response = new HashMap<>();

    if (result.hasErrors()){
      //List<String> errors = new ArrayList<>();
      //for (FieldError err: result.getFieldErrors()){
      //  errors.add("El campo '" + err.getField() + "' " + err.getDefaultMessage());
      //}
      List<String> errors =  result.getFieldErrors()
              .stream()
              .map(err -> { return "El campo '" + err.getField() + "' " + err.getDefaultMessage();})
              .collect(Collectors.toList());

      response.put("errors", errors);
      return  new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
    }
    try {
      clienteNew = clienteService.save(cliente);
    } catch (DataAccessException e) {
      response.put("mensaje", "Error al insertar en la base de datos!");
      response.put("error", e.getMessage().concat(": ".concat(e.getMostSpecificCause().getMessage())));
      return  new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    response.put("mensaje", "El cliente ha sido creado con éxito");
    response.put("cliente", clienteNew);
    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
  }

  @Secured("ROLE_ADMIN")
  @PutMapping("/clientes/{id}")
  public ResponseEntity<?> updateCliente(@Valid @RequestBody Cliente cliente, BindingResult result,  @PathVariable Long id) {
    Cliente clienteActual = clienteService.findById(id);
    Cliente clienteUpdated = null;
    Map<String, Object> response = new HashMap<>();

    if (result.hasErrors()){
      List<String> errors = result.getFieldErrors()
              .stream()
              .map(err -> "El campo '"+ err.getField() + "' "+ err.getDefaultMessage())
              .collect(Collectors.toList());
      response.put("errors", errors);
      return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
    }

    if (clienteActual == null){
      response.put("mensaje", "No se puede editar el cliente con ID ".concat(id.toString()).concat(". No existe en la base de datos"));
      return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
    }

    try {
      clienteActual.setApellido(cliente.getApellido());
      clienteActual.setNombre(cliente.getNombre());
      clienteActual.setEmail(cliente.getEmail());
      System.out.println(cliente.getCreateAt());
      clienteActual.setCreateAt(cliente.getCreateAt());

      clienteUpdated = clienteService.save(clienteActual);
    } catch (DataAccessException e) {
      response.put("mensaje", "Error al realizar el update en la base de datos");
      response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
      return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    response.put("mensaje", "El cliente ha sido actualizado con éxito");
    response.put("cliente", clienteUpdated);
    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.ACCEPTED);
  }

  @Secured("ROLE_ADMIN")
  @DeleteMapping("clientes/{id}")
  public ResponseEntity<?> deleteCliente(@PathVariable Long id){
    Map<String, Object> response = new HashMap<>();

    try {
      Cliente cliente = clienteService.findById(id);
      String nombreFotoAnterior = cliente.getPhoto();

      uploadFileService.delete(nombreFotoAnterior);

      clienteService.delete(id);
    } catch (DataAccessException e) {
      response.put("mensaje", "Error al eliminar el cliente de la base de datos");
      response.put("error", e.getMessage());
      return new ResponseEntity<Map<String, Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
    }
    response.put("mensaje", "Cliente con ID ".concat(id.toString()).concat(" eliminado exitosamente"));
    return new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
  }

  @Secured({"ROLE_ADMIN","ROLE_USER"})
  @PostMapping("/clientes/upload")
  public ResponseEntity<?> upload( @RequestParam("archivo") MultipartFile file, @RequestParam("id") Long id ){
    Map<String, Object> response = new HashMap<>();
    Cliente cliente = clienteService.findById(id);

    if (!file.isEmpty()){
      String nombreArchivo = null;
      try {
        nombreArchivo = uploadFileService.copy(file);
      } catch (IOException e) {
        response.put("mensaje", "Error al subir la imagen del cliente");
        response.put("error", e.getMessage().concat(": ").concat(e.getCause().getMessage()));
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
      }

      String nombreFotoAnterior = cliente.getPhoto();
      uploadFileService.delete(nombreFotoAnterior);

      cliente.setPhoto(nombreArchivo);
      clienteService.save(cliente);
      response.put("cliente", cliente);
      response.put("mensaje", "Imagen "+ nombreArchivo +" subida correctamente!");
    }
    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
  }

  /**@GetMapping("/clientes/img/{nombreFoto:.+}")
  public ResponseEntity<Resource> verFoto(@PathVariable String nombreFoto){
    Resource recurso = null;

    try {
      recurso = (Resource) uploadFileService.load(nombreFoto);
    }catch (MalformedURLException e){
      e.printStackTrace();
    }

    if (!recurso.exists() && !recurso.isReadable)){
      rutaArchivo = Paths.get("src/main/resources/static/images").resolve("no-usuario.png").toAbsolutePath();

      try{
        recurso = (Resource) new UrlResource(rutaArchivo.toUri());
      } catch (MalformedURLException e){
        e.printStackTrace();
      }
      log.error("No se pudo cargar la imagen: " +nombreFoto);
    }
  }*/
}
