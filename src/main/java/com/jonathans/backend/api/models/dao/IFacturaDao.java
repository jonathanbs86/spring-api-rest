package com.jonathans.backend.api.models.dao;

import com.jonathans.backend.api.models.entity.Factura;
import org.springframework.data.repository.CrudRepository;

public interface IFacturaDao extends CrudRepository<Factura, Long> {
}
